# Dishes Form App

Node version: v18.13.0
NPM version: 8.19.3

## Setup

Run the following commands to setup the project and start the server in development mode. The server will be running on port 3000.

```bash
    npm install
    npm run start
```

### Time it took to complete the task

- 1 hour to setup the project
- 3 hours to complete the task
