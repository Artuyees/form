import { combineReducers, createStore } from "redux";
import { reducer as reduxFormReducer } from "redux-form";

const initialState = {
  loading: false,
  data: null,
  error: null,
};

function dataReducer(state = initialState, action: any) {
  switch (action.type) {
    case "LOADING_START":
      return {
        ...state,
        loading: true,
      };
    case "LOADING_SUCCESS":
      return {
        ...state,
        loading: false,
        data: action.payload,
      };
    case "LOADING_ERROR":
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}

const reducer = combineReducers({
  dataReducer: dataReducer,
  form: reduxFormReducer,
});

let store = createStore(reducer);

export default store;
