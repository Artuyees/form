import TextField from "@material-ui/core/TextField";
import React from "react";
import { CustomFieldProps } from "../types";

const CustomTextField = ({
  input,
  label,
  meta: { touched, error },
  ...custom
}: CustomFieldProps) => (
  <TextField
    style={{ width: "100%" }}
    label={label}
    error={touched && !!error}
    helperText={touched && error}
    {...input}
    {...custom}
  />
);

export default CustomTextField;
