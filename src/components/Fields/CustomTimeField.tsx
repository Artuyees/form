import TextField from "@material-ui/core/TextField";
import React from "react";
import { CustomFieldProps } from "../types";
import TimeField from "react-simple-timefield";

const CustomTimeField = ({
  input,
  label,
  meta: { touched, error },
  ...custom
}: CustomFieldProps) => {
  return (
    <TimeField
      style={{ width: "100%" }}
      showSeconds
      {...input}
      onChange={(value) => input.onChange(value)}
      {...custom}
      input={
        <TextField
          error={touched && !!error}
          label={label}
          helperText={touched && error}
          onChange={(value) => input.onChange(value)}
        />
      }
    />
  );
};

export default CustomTimeField;
