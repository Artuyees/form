import { useEffect, useState } from "react";
import { CustomFieldProps } from "../types";
import { Slider, Typography } from "@material-ui/core";

const CustomSliderField = ({
  input,
  label,
  meta: { touched, error },
  ...custom
}: CustomFieldProps) => {
  const [innerValue, setInnerValue] = useState(1);

  // Simple hack to make the slider work
  useEffect(() => {
    input.onChange(innerValue);
  }, [innerValue, input]);

  return (
    <>
      <Typography gutterBottom>{label}</Typography>
      <Slider
        style={{ width: "100%" }}
        defaultValue={1}
        onChange={(e, value) => setInnerValue(value as number)}
        max={10}
        min={1}
        marks
        // {...input}
        step={1}
        {...custom}
      />
    </>
  );
};

export default CustomSliderField;
