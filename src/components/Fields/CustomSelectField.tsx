import {
  FormControl,
  FormHelperText,
  InputLabel,
  Select,
} from "@material-ui/core";
import { CustomFieldProps } from "../types";

const CustomSelectField = ({
  input,
  label,
  meta: { touched, error },
  children,
  ...custom
}: CustomFieldProps) => (
  <FormControl error={touched && !!error} style={{ width: "100%" }}>
    <InputLabel>{label}</InputLabel>
    <Select
      {...input}
      error={touched && !!error}
      children={children}
      {...custom}
    />
    <FormHelperText>{touched && error}</FormHelperText>
  </FormControl>
);

export default CustomSelectField;
