import { Grid } from "@material-ui/core";
import { Field } from "redux-form";
import CustomTextField from "../Fields/CustomTextField";
import CustomSliderField from "../Fields/CustomSliderField";
import { ChosenType } from "../types";

type Props = {
  chosenType: "pizza" | "sandwich" | "soup";
};
const DynamicFields = ({ chosenType }: Props) => {
  switch (chosenType) {
    case ChosenType.pizza:
      return (
        <>
          <Grid item xs={12}>
            <Field
              name="no_of_slices"
              component={CustomTextField}
              label="Number of slices"
              type="number"
            />
          </Grid>
          <Grid item xs={12}>
            <Field
              name="diameter"
              component={CustomTextField}
              label="Diameter"
              type="number"
            />
          </Grid>
        </>
      );

    case ChosenType.sandwich:
      return (
        <Grid item xs={12}>
          <Field
            name="slices_of_bread"
            type="number"
            component={CustomTextField}
            label="Slices of bread"
          />
        </Grid>
      );
    case ChosenType.soup:
      return (
        <Grid item xs={12}>
          <Field
            name="spiciness_scale"
            component={CustomSliderField}
            label="Spiciness scale"
            type="number"
          />
        </Grid>
      );

    default:
      return null;
  }
};

export default DynamicFields;
