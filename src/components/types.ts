export type CustomFieldProps = {
  input: any;
  label: string;
  meta: {
    touched: boolean;
    error: string;
  };
  [key: string]: any;
};

export enum ChosenType {
  pizza = "pizza",
  soup = "soup",
  sandwich = "sandwich",
}
