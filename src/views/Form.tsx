import React from "react";
import { Field, formValueSelector, reduxForm } from "redux-form";
import CustomSelectField from "../components/Fields/CustomSelectField";
import CustomTextField from "../components/Fields/CustomTextField";
import { validate } from "../utils/validate";
import CustomTimeField from "../components/Fields/CustomTimeField";
import { Button, Container, Grid, MenuItem, Paper } from "@material-ui/core";
import { connect } from "react-redux";
import DynamicFields from "../components/DynamicFields/DynamicFields";

let Form = (props: any) => {
  const { error, handleSubmit, pristine, submitting, chosenType } = props;

  return (
    <Container maxWidth="sm" style={{ marginTop: "2rem" }}>
      <form onSubmit={handleSubmit}>
        <Paper style={{ padding: "1rem" }}>
          <Grid container spacing={3} justifyContent="center">
            <Grid item xs={12}>
              <Field
                name="name"
                component={CustomTextField}
                label="Name of the dish"
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                name="preparation_time"
                component={CustomTimeField}
                label="Preparation time"
              />
            </Grid>
            <Grid item xs={12}>
              <Field
                name="type"
                defaultValue="pizza"
                component={CustomSelectField}
                label="Dish type"
              >
                <MenuItem value="pizza">Pizza</MenuItem>
                <MenuItem value="soup">Soup</MenuItem>
                <MenuItem value="sandwich">Sandwich</MenuItem>
              </Field>
            </Grid>

            <DynamicFields chosenType={chosenType} />

            <Grid item xs={12}>
              <Grid container justifyContent="center">
                <Button
                  size="large"
                  variant="contained"
                  color="primary"
                  type="submit"
                  disabled={pristine || submitting}
                >
                  {props.loading ? "Loading..." : "Submit"}
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </form>
      {error && <strong>{error}</strong>}
    </Container>
  );
};

const createReduxForm = reduxForm({
  form: "dishesForm",
  validate,
});

const selector = formValueSelector("dishesForm");

const connectedRedux = connect((state) => {
  const chosenType = selector(state, "type");

  return {
    chosenType,
  };
})(Form);

const dishesForm = createReduxForm(connectedRedux);

const mapStateToProps = (state: any) => ({
  loading: state.dataReducer.loading,
  data: state.dataReducer.data,
  error: state.dataReducer.error,
});

const connnectedLogic = connect(mapStateToProps)(dishesForm);

export default connnectedLogic;
