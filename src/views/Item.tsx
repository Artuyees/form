import { Container, Grid, Paper, Typography } from "@material-ui/core";

import { connect } from "react-redux";

const ItemDisplay = (props: any) => {
  return (
    <Container maxWidth="sm" style={{ marginTop: "2rem" }}>
      <Paper style={{ padding: "1rem" }}>
        <Grid container spacing={3} justifyContent="center">
          {Object.keys(props.data).map((item: any) => (
            <Grid item xs={12}>
              <Typography variant="h6">
                {item}: {props.data[item]}
              </Typography>
            </Grid>
          ))}
        </Grid>
      </Paper>
    </Container>
  );
};

const mapStateToProps = (state: any) => ({
  data: state.dataReducer.data,
});

const ItemWithConnectedRedux = connect(mapStateToProps)(ItemDisplay);

export default ItemWithConnectedRedux;
