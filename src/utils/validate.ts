import { ChosenType } from "../components/types";

type Props = {
  name: string;
  preparation_time: string;
  type: "pizza" | "soup" | "sandwich";
  no_of_slices: number;
  diameter: number;
  spiciness_scale: number;
  slices_of_bread: number;
};

export const validate = (values: Props) => {
  const TYPES = ["pizza", "soup", "sandwich"];
  const errors: Record<keyof Props, string> = {
    name: "",
    preparation_time: "",
    type: "",
    no_of_slices: "",
    diameter: "",
    spiciness_scale: "",
    slices_of_bread: "",
  };

  if (!values.name) {
    errors.name = "Name is required";
  }

  if (!values.preparation_time || values.preparation_time === "00:00:00") {
    errors.preparation_time = "Preparation time is required";
  }

  if (!values.type) {
    errors.type = "Type is required";
  } else if (TYPES.includes(values.type) === false) {
    errors.type = "Type is invalid";
  }

  //check if any value is less than zero
  if (values.no_of_slices <= 0) {
    errors.no_of_slices = "Number of slices cannot be less than zero";
  }

  if (values.diameter <= 0) {
    errors.diameter = "Diameter cannot be less than zero";
  }

  if (values.spiciness_scale < 0) {
    errors.spiciness_scale = "Spiciness scale cannot be less than zero";
  }

  if (values.slices_of_bread <= 0) {
    errors.slices_of_bread = "Slices of bread cannot be less than zero";
  }

  if (values.type === ChosenType.pizza && !values.no_of_slices) {
    errors.no_of_slices = "Number of slices is required";
  }

  if (values.type === ChosenType.pizza && !values.diameter) {
    errors.diameter = "Diameter is required";
  }

  if (values.type === ChosenType.soup && !values.spiciness_scale) {
    errors.spiciness_scale = "Spiciness scale is required";
  }

  if (values.type === ChosenType.sandwich && !values.slices_of_bread) {
    errors.slices_of_bread = "Slices of bread is required";
  }

  return errors;
};
