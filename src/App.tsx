import { connect } from "react-redux";
import Form from "./views/Form";
import Item from "./views/Item";

function App(props: any) {
  const Submit = async (values: any) => {
    props.startLoading();
    let response = await fetch(
      "https://umzzcc503l.execute-api.us-west-2.amazonaws.com/dishes/",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(values),
      }
    );
    const data = await response.json();

    if (response.ok) {
      props.loadSuccess(data);
    }

    if (!response.ok) {
      props.loadError(data);

      alert(
        Object.keys(data).reduce((acc: any, key: any) => {
          return acc + key + ": " + data[key] + "\n";
        }, "")
      );
    }
  };

  if (props.data) {
    return <Item />;
  }

  return <Form onSubmit={Submit} />;
}

const mapStateToProps = (state: any) => ({
  loading: state.dataReducer.loading,
  data: state.dataReducer.data,
  error: state.dataReducer.error,
});

const mapDispatchToProps = (dispatch: any) => ({
  startLoading: () => dispatch({ type: "LOADING_START" }),
  loadSuccess: (data: any) =>
    dispatch({ type: "LOADING_SUCCESS", payload: data }),
  loadError: (error: any) =>
    dispatch({ type: "LOADING_ERROR", payload: error }),
});

const AppWithConnectedRedux = connect(mapStateToProps, mapDispatchToProps)(App);

export default AppWithConnectedRedux;
